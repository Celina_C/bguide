import functools

from functools import wraps

from bguide import db
from bguide.models.beacon import Beacon
from bguide.models.statistics import Statistics


def beacon_exists(f):
    @functools.wraps(f)
    def wrapped(self, uuid, *args, **kwargs):
        beacon = Beacon.get_by_uuid(uuid)
        if not beacon:
            abort(400)
        return f(self, beacon, *args, **kwargs)
    return wrapped

def statistics(f):
    @functools.wraps(f)
    def wrapped(self, beacon, *args, **kwargs):
        statistic = Statistics.get_by_beacon_id(beacon.id)
        if statistic:
            statistic.value = statistic.value + 1
            db.session.add(statistic)
            db.session.commit()
        else:
            Statistics.create(beacon_id=beacon.id)
        return f(self, beacon, *args, **kwargs)
    return wrapped
