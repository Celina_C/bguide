from bguide import app
from bguide.api import beacon, information, photo


# Beacon
app.add_url_rule('/api/beacons',
                 view_func=beacon.BeaconsAPI.as_view('beacons_api'),
                 methods=['GET'])
app.add_url_rule('/api/beacon/<uuid>',
                 view_func=beacon.BeaconAPI.as_view('beacon_api'),
                 methods=['GET'])

# Information
app.add_url_rule('/api/beacon/<uuid>/information',
                 view_func=information.InformationAPI.as_view('information'),
                 methods=['GET'])

# Photo
app.add_url_rule('/api/photo',
				 view_func=photo.PhotoAPI.as_view('api_photo'),
				 methods=['GET'])
