from marshmallow import fields, Schema


class BeaconSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str()
    uuid = fields.UUID()
    long_position = fields.Float()
    lat_position = fields.Float()


beacons_schema = BeaconSchema(many=True)
beacon_schema = BeaconSchema()
