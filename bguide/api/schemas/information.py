from marshmallow import Schema, fields, ValidationError

from bguide.api.schemas.beacon import BeaconSchema
from bguide.models.beacon import Beacon
from .photo import PhotoSchema
from bguide.api.schemas.validators.validator import *


class InformationSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(validate=len_validator)
    content = fields.Str(validate=len_validator)
    beacon = fields.Nested(BeaconSchema, validate=beacon_validator)
    photo = fields.Nested(PhotoSchema, validate=photo_validator)

information_schema = InformationSchema()
