from marshmallow import ValidationError

from bguide.models.beacon import Beacon
from bguide.models.information import Photo


def beacon_validator(beacon):
    if beacon:
        if not Beacon.get_by_id(beacon.id):
            raise ValidationError('Beacon doesn\'t exists.')

def photo_validator(photo):
    if photo:
        if not Photo.get_by_id(photo.id):
            raise ValidationError('Photo doesn\'t exists.')

def len_validator(data):
    if len(data) < 2:
        raise ValidationError('Too small word.')
