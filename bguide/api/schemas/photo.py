from marshmallow import Schema, fields, ValidationError

from bguide.api.schemas.validators.validator import *


class PhotoSchema(Schema):
    id = fields.Int(dump_only=True)
    filename = fields.Str(validate=len_validator)
    img = fields.Str(validate=len_validator)