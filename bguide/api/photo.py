import base64

from flask import abort, jsonify
from flask.views import MethodView

from bguide.models.photo import Photo
from .schemas.photo import PhotoSchema


class PhotoAPI(MethodView):
    def get(self):
        '''Gets photos base information'''
        photo = Photo.query.first()
        photo.img = photo_convert(photo.img)
        schema = PhotoSchema()
        result = schema.dump(photo)
        return jsonify(result.data)


def photo_convert(img):
    return base64.b64encode(img)