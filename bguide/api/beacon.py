from flask import jsonify
from flask.views import MethodView

from bguide.models.beacon import Beacon
from .schemas.beacon import *


class BeaconsAPI(MethodView):

    def get(self):
        ''' Gets beacons list'''
        beacons = Beacon.query.all()
        result = beacons_schema.dump(beacons)
        return jsonify({'items': result.data})


class BeaconAPI(MethodView):

    def get(self, uuid):
        ''' Gets beacon base data'''
        beacon = Beacon.get_by_uuid(uuid)
        result = beacon_schema.dump(beacon)
        return jsonify(result.data)
