from flask import abort, jsonify
from flask.views import MethodView

from bguide.models.beacon import Beacon
from bguide.models.information import Information
from bguide.api.photo import photo_convert
from bguide.decorators import beacon_exists, statistics
from .schemas.information import information_schema


class InformationAPI(MethodView):
    @beacon_exists
    @statistics
    def get(self, beacon):
        '''Gets beacons information'''
        information = Information.get_by_beacon_id(beacon.id)
        information.photo.img = photo_convert(information.photo.img)
        result = information_schema.dump(information)
        return jsonify(result.data)
