import datetime

from werkzeug.security import generate_password_hash, \
    check_password_hash

from bguide import db, login_manager


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    is_active = db.Column(db.Boolean(), default=False)
    create_at = db.Column(db.DateTime, nullable=False)

    def str(self):
        return self.email

    @classmethod
    def get_by_email(cls, user_email):
        return cls.query.filter(cls.email == user_email).first()

    @classmethod
    def create(cls, **data):
        user = User(
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
            email=data.get('email'),
            password=generate_password_hash(data.get('password')),
            create_at=datetime.datetime.now(),
            is_active=True
        )
        db.session.add(user)
        db.session.commit()
        return user

    def check_password(self, check_password):
        return check_password_hash(self.password, check_password)

    def is_admin(self):
        self.admin = True

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def get_id(self):
        return self.id

    @login_manager.user_loader
    def load_user(userid):
        return User.query.get(userid)
