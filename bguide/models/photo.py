from bguide import db


class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(50))
    img = db.Column(db.LargeBinary)

    def __str__(self):
        return self.filename

    @classmethod
    def get_by_id(cls, id):
        return cls.query.filter(cls.id == id).first()

    @classmethod
    def create(cls, **data):
        new_img = data.get('img')
        photo = cls(
            filename=data.get('filename'),
            img=new_img.read(),
        )
        new_img.close()
        db.session.add(photo)
        db.session.commit()
        return photo
