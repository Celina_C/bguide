from bguide import db


class Beacon(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    uuid = db.Column(db.String(50))
    long_position = db.Column(db.Float)
    lat_position = db.Column(db.Float)

    def __str__(self):
        return self.name

    @classmethod
    def get_by_id(cls, id):
        return cls.query.filter(cls.id == id).first()

    @classmethod
    def get_by_uuid(cls, uuid):
        return cls.query.filter(cls.uuid == uuid).first()

    @classmethod
    def create(cls, **data):
        beacon = cls(
            name=data.get('name'),
            uuid=data.get('uuid'),
            long_position=data.get('long_position'),
            lat_position=data.get('lat_position')
        )
        db.session.add(beacon)
        db.session.commit()
        return beacon
