from bguide import db
from bguide.models.beacon import Beacon
from bguide.models.photo import Photo


class Information(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    content = db.Column(db.String(500))
    beacon_id = db.Column(db.Integer, db.ForeignKey(Beacon.id))
    beacon = db.relationship(Beacon,  backref='information')
    photo_id = db.Column(db.Integer, db.ForeignKey(Photo.id))
    photo = db.relationship(Photo, backref='photo')

    def __str__(self):
        return self.name

    @classmethod
    def get_by_name(cls, title):
        return cls.query.filter(cls.title == title).first()

    @classmethod
    def get_by_beacon_id(cls, beacon_id):
        return cls.query.filter(cls.beacon_id == beacon_id).first()

    @classmethod
    def create(cls, beacon_id=0, photo_id=0, **data):
        info = cls(
            title=data.get('title'),
            content=data.get('content'),
            beacon_id=beacon_id,
            photo_id=photo_id
        )
        db.session.add(info)
        db.session.commit()
        return info
