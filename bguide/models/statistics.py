from bguide import db

from bguide.models.beacon import Beacon


class Statistics(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    beacon_id = db.Column(db.Integer, db.ForeignKey(Beacon.id))
    beacon = db.relationship(Beacon,  backref='statistics')
    value = db.Column(db.Integer, default=0)

    def __str__(self):
        return self.value

    @classmethod
    def get_by_beacon_id(cls, beacon_id):
        return cls.query.filter(cls.beacon_id == beacon_id).first()

    @classmethod
    def create(cls, beacon_id=0):
        statistic = cls(
            beacon_id=beacon_id,
            value=1
        )
        db.session.add(statistic)
        db.session.commit()
        return statistic
