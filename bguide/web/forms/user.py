from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, validators
from wtforms.fields.html5 import EmailField


class RegisterForm(Form):
    first_name = StringField(u'First Name', [validators.Length(min=2, max=50)])
    last_name = StringField(u'Last Name', [validators.Length(min=2, max=50)])
    email = EmailField(u'Email',
                       [validators.DataRequired(), validators.Email()])
    password = PasswordField(u'New Password',
                             [validators.Length(min=2, max=50)])
    repeat_password = PasswordField(u'Repeat Password',
                                    [validators.Length(min=2, max=50)])


class LoginForm(Form):
    email = EmailField(u'Email',
                       [validators.DataRequired(), validators.Email()])
    password = PasswordField(u'Passowrd', [validators.Length(min=2, max=50)])
