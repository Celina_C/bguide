import re

from flask.ext.wtf import Form
from wtforms import StringField, FloatField, validators
from wtforms.validators import Required, ValidationError


UUID_PATERN = re.compile(
    '[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}')


def uuid_validators(form, filed):
    if not UUID_PATERN.match(filed.data):
        raise ValidationError("UUID number is not valid.")


class BeaconForm(Form):
    uuid = StringField(u'UUID', validators=[Required(), uuid_validators])
    name = StringField(u'Name', [validators.Length(min=2, max=30)])
    long_position = FloatField('Long Position', [Required()])
    lat_position = FloatField('Long Position', [Required()])
