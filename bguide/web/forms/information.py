from flask.ext.wtf import Form
from wtforms import StringField,  validators
from wtforms.validators import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from bguide.models.beacon import Beacon
from bguide.models.information import Photo, Information


def beacon_validators(form, fields):
    beacon_id = form.data['beacon'].id
    if Information.get_by_beacon_id(beacon_id):
        raise ValidationError("Information exists for this Beacon.")


class InformationForm(Form):
    title = StringField(u'Title', [validators.Length(min=2, max=50)])
    content = StringField(u'Content', [validators.Length(min=2, max=500)])
    beacon = QuerySelectField(query_factory=Beacon.query.all,
                              get_pk=lambda a: a.id,
                              get_label=lambda a: a.name,
                              validators=[beacon_validators])
    photo = QuerySelectField(query_factory=Photo.query.all,
                             get_pk=lambda a: a.id,
                             get_label=lambda a: a.filename)
