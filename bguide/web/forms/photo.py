from flask.ext.wtf import Form
from flask_wtf.file import FileField
from wtforms import TextField,  validators
from wtforms.validators import Required


class PhotoForm(Form):
    filename = TextField('FileName', [validators.Length(min=2, max=50)])
    img = FileField('Image File', [Required()])
    # TODO add validators
