from bguide import app
from bguide.web import beacon, index, information, statistics, photo, user


app.add_url_rule('/', 'home',
                 view_func=index.home, methods=['GET'])

# User
app.add_url_rule('/web/register',
                 'register',
                 view_func=user.user_create,
                 methods=['GET', 'POST'])
app.add_url_rule('/login',
                 'login',
                 view_func=user.login,
                 methods=['GET', 'POST'])
app.add_url_rule('/logout',
                 'logout',
                 view_func=user.logout,
                 methods=['GET'])

# Beacon
app.add_url_rule('/web/beacon',
                 'beacon_list',
                 view_func=beacon.beacon_list,
                 methods=['GET'])
app.add_url_rule('/web/beacon/<int:beacon_id>/delete',
                 'beacon_delete',
                 view_func=beacon.beacon_delete,
                 methods=['GET'])
app.add_url_rule('/web/beacon/add', 'beacon_create',
                 view_func=beacon.beacon_create,
                 methods=['GET', 'POST'])

# Information
app.add_url_rule('/web/information',
                 view_func=information.information_list,
                 methods=['GET'])
app.add_url_rule('/web/information/<int:information_id>',
                 view_func=information.information_detail,
                 methods=['GET'])
app.add_url_rule('/web/information/<int:information_id>/delete',
                 'information_delete',
                 view_func=information.information_delete,
                 methods=['GET'])
app.add_url_rule('/web/information/add',
                 view_func=information.information_create,
                 methods=['GET', 'POST'])

# Photo
app.add_url_rule('/web/photo',
                 view_func=photo.photo_list,
                 methods=['GET'])
app.add_url_rule('/web/photo/add',
                 view_func=photo.photo_create,
                 methods=['GET','POST'])
app.add_url_rule('/web/photo/<int:photo_id>',
                 view_func=photo.photo,
                 methods=['GET'])
app.add_url_rule('/web/photo/<int:photo_id>/delete',
                 view_func=photo.photo_delete,
                 methods=['GET'])

# Statistics
app.add_url_rule('/web/statistics',
                 view_func=statistics.statistics_list,
                 methods=['GET'])

