from flask import redirect, render_template, url_for
from flask.views import MethodView

from bguide import app, db
from bguide.web.forms.photo import PhotoForm
from bguide.models.beacon import Beacon
from bguide.models.photo import Photo


def photo_list():
    query = Photo.query.all()
    return render_template("information/photo.html", photos=query)

def photo(photo_id):
    query = Photo.query.get_or_404(photo_id)
    return app.response_class(query.img, mimetype='application/octet-stream')

def photo_create():
    form = PhotoForm()
    if form.validate_on_submit():
        Photo.create(**form.data)
        return redirect(url_for('home'))
    return render_template("information/photo_form.html", form=form)

def photo_delete(photo_id):
    query = Photo.query.get_or_404(photo_id)
    db.session.delete(query)
    db.session.commit()
    return redirect(url_for('photo_list'))
