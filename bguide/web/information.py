from flask import redirect, render_template, url_for

from bguide import app, db
from bguide.web.forms.information import InformationForm
from bguide.models.beacon import Beacon
from bguide.models.information import Information
from bguide.models.photo import Photo


def information_list():
    query = Information.query.all()
    return render_template("information/information.html", informations=query)

def information_detail(information_id):
    query = Information.query.get_or_404(information_id)
    return render_template("information/information_detail.html", information=query)

def information_create():
    form = InformationForm()

    if form.validate_on_submit():
        beacon_form = str(form.beacon.data)
        beacon = Beacon.query.filter_by(name=beacon_form).first_or_404()

        photo_form = str(form.photo.data)
        photo = Photo.query.filter_by(filename=photo_form).first_or_404()

        Information.create(beacon_id=beacon.id, photo_id=photo.id, **form.data)
        return redirect(url_for('information_list'))

    return render_template("information/form.html", form=form)

def information_delete(information_id):
    query = Information.query.get_or_404(information_id)
    db.session.delete(query)
    db.session.commit()
    return redirect(url_for('information_list'))
