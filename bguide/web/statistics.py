from flask import render_template

from bguide import db
from bguide.models.beacon import Beacon
from bguide.models.information import Information
from bguide.models.statistics import Statistics


def statistics_list():
    query = db.session.query(Statistics.value,
                             Beacon.name.label('beacon'),
                             Information.id.label('info_id'),
                             Information.title.label('info_title'))\
        .join(Beacon, Beacon.id == Statistics.beacon_id)\
        .join(Information, Information.beacon_id == Statistics.beacon_id)\
        .order_by(-Statistics.value)\
        .all()
    return render_template("statistics.html", statistics=query)
