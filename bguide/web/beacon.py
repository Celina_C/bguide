from flask import redirect, render_template, url_for

from bguide import db
from .forms.beacon import *
from bguide.models.beacon import Beacon
from bguide.models.information import Information


def beacon_list():
    query = Beacon.query.all()
    return render_template("beacon/beacon.html", beacons=query)


def beacon_create():
    form = BeaconForm()
    if form.validate_on_submit():
        Beacon.create(**form.data)
        return redirect(url_for('home'))
    return render_template("beacon/form.html", form=form)


def beacon_delete(beacon_id):
    query = Beacon.query.get_or_404(beacon_id)
    db.session.delete(query)
    db.session.commit()
    return redirect(url_for('beacon_list'))
