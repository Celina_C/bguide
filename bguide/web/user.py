import base64
from flask import flash, redirect, render_template, request, url_for
from flask.ext.login import login_user, logout_user, current_user

from bguide import db
from bguide.models.user import User
from bguide.web.forms.user import RegisterForm, LoginForm


def user_create():
    form = RegisterForm()
    if form.validate_on_submit():
        if not form.data['password'] == form.data['repeat_password']:
            flash(u"Please, check your password.")
            return render_template("user/form.html", form=form)
        User.create(**form.data)
        return redirect(url_for('home'))
    return render_template("user/form.html", form=form)


def login():
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit:
        user = User.get_by_email(form.data['email'])
        if not user:
            flash(u"Please, check your email.")
            return render_template("user/login.html", form=form)
        if not user.check_password(form.data['password']):
            flash(u"Please, check your password.")
            return render_template("user/login.html", form=form)

        login_user(user, remember=True)
        print(current_user)
        return redirect(url_for('home'))
    return render_template("user/login.html", form=form)


def logout():
    logout_user()
    return redirect('/login')
