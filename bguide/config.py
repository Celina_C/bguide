import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = True
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'caaaaaa'
    # SQLALCHEMY_DATABASE_URI = 'postgresql://celina@localhost:5432/bguide_test'
    SQLALCHEMY_DATABASE_URI = 'postgresql://celina@localhost:5432/bguide'


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    CSRF_ENABLED = True
    SECRET_KEY = 'cdscds'
    SQLALCHEMY_DATABASE_URI = 'postgresql://celina@localhost:5432/bguide_test'
