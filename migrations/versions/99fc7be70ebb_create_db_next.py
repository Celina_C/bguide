"""create db next

Revision ID: 99fc7be70ebb
Revises: 81c75306022f
Create Date: 2016-04-25 18:42:40.485455

"""

# revision identifiers, used by Alembic.
revision = '99fc7be70ebb'
down_revision = '81c75306022f'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('beacon',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.Column('uuid', sa.String(length=50), nullable=True),
    sa.Column('long_position', sa.Float(), nullable=True),
    sa.Column('lat_position', sa.Float(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('information',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=50), nullable=True),
    sa.Column('content', sa.String(length=500), nullable=True),
    sa.Column('beacon_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['beacon_id'], ['beacon.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('information')
    op.drop_table('beacon')
    ### end Alembic commands ###
