"""empty message

Revision ID: 67e722effc48
Revises: d47b59ce4dda
Create Date: 2016-07-09 18:38:27.774571

"""

# revision identifiers, used by Alembic.
revision = '67e722effc48'
down_revision = 'd47b59ce4dda'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('beacon',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.Column('uuid', sa.String(length=50), nullable=True),
    sa.Column('long_position', sa.Float(), nullable=True),
    sa.Column('lat_position', sa.Float(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('information',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=50), nullable=True),
    sa.Column('content', sa.String(length=500), nullable=True),
    sa.Column('beacon_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['beacon_id'], ['beacon.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('information')
    op.drop_table('beacon')
    ### end Alembic commands ###
