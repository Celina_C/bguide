from flask.ext.script import Command
from sqlalchemy.sql import exists

from bguide import app, db
from bguide.models.beacon import Beacon
from bguide.models.information import Information


class InsertRecords(Command):

    def run(self):
        beacon1 = Beacon(
            name="GIcR",
            uuid="f7826da6-4fa2-4e98-8024-bc5b71e0893d",
            long_position=51.11,
            lat_position=19.11
        )
        beacon2 = Beacon(
            name="P1yH",
            uuid="f7826da6-4fa2-4e98-8024-bc5b71e0893f",
            long_position=51.12,
            lat_position=19.12
        )
        beacon3 = Beacon(
            name="PVOl",
            uuid="f7826da6-4fa2-4e98-8024-bc5b71e0893f",
            long_position=51.13,
            lat_position=19.13
        )
        beacon4 = Beacon(
            name="biqp",
            uuid="f7826da6-4fa2-4e98-8024-bc5b71e0893e",
            long_position=51.14,
            lat_position=19.14
        )

        all_beacon = [beacon1, beacon2, beacon3, beacon4]

        for beacon in all_beacon:
            beacon_exists = db.session.query(
                db.exists().where(Beacon.name == beacon.name)).scalar()
            if not beacon_exists:
                print("Add new beacon")
                db.session.add(beacon)
                db.session.commit()

        info1 = Information(
            title="cosik1",
            content="cosik1",
            beacon_id=beacon1.id
        )

        info2 = Information(
            title="cosik2",
            content="cosik2",
            beacon_id=beacon2.id
        )
        info3 = Information(
            title="cosik3",
            content="cosik3",
            beacon_id=beacon3.id
        )
        info4 = Information(
            title="cosik4",
            content="cosik4",
            beacon_id=beacon4.id
        )

        all_info = [info1, info2, info3, info4]

        for info in all_info:
            info_exists = db.session.query(db.exists().where(
                Information.beacon_id == info.beacon_id)).scalar()
            if not info_exists and info.beacon_id != None:
                print("Add new information")
                db.session.add(info)
                db.session.commit()
