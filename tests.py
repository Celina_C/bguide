import os
import unittest

from coverage import coverage

from tests.api.test_beacons import *
from tests.api.test_information import *
from tests.api.test_photo import *
from tests.models.test_model_beacon import *
from tests.models.test_model_information import *
from tests.models.test_model_photo import *
from tests.models.test_model_user import *
from tests.web.test_beacon import *
from tests.web.test_information import *
from tests.web.test_user import *

my_coverage = coverage(branch=True, omit=['/Users/celina/Projects/bguide/lib/*', 'tests/*','tests.py'])
my_coverage.start()

def stop_my_coverage():
    my_coverage.stop()
    my_coverage.save()
    print("\n\nCoverage Report:\n")
    my_coverage.report()
    my_coverage.html_report(directory='tmp/coverage')
    my_coverage.erase()

if __name__ == '__main__':
    try:
        unittest.main()
    except:
        pass
    stop_my_coverage()