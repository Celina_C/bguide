from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.script import Manager, Server

from bguide import app, db
from script import InsertRecords

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server())
manager.add_command('script', InsertRecords())

if __name__ == "__main__":
    manager.run()
