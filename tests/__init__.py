import unittest

from io import BytesIO
from PIL import Image
from flask.ext.webtest import TestApp

from bguide import app, db


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.app = TestApp(app, db=db, use_session_scopes=True)
        self.app_context = self.app.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @classmethod
    def setUpClass(cls):
        db.create_all()
        cls.load_data()

    @classmethod
    def tearDownClass(cls):
        db.session.close_all()
        db.drop_all()

    @staticmethod
    def load_data():
        pass

def create_test_image():
    file = BytesIO()
    image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
    image.save(file, 'png')
    file.name = 'test.png'
    file.seek(0)
    return file
