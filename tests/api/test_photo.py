from bguide.models.photo import Photo
from bguide.api.photo import photo_convert
from tests import BaseTestCase, create_test_image


class PhotoTestCase(BaseTestCase):
    filename = "sukiennice"
    img = create_test_image().read()

    def test_photo(self):
        photo = Photo(
            filename=self.filename,
            img=self.img
        )
        self.app.db.session.add(photo)
        self.app.db.session.commit()

        rv = self.app.get("/api/photo")
        self.assertEqual(rv.json['filename'], self.filename)
        self.assertTrue(rv.json['img'])
