from bguide.models.beacon import Beacon
from bguide.models.information import Information
from bguide.models.photo import Photo
from tests import BaseTestCase, create_test_image


class InformationTestCase(BaseTestCase):
    info_title = 'title'
    beacon_name = 'beacon_b1'
    beacon_uuid = '65c0cae2-0571-11e6-b512-3e1d05defe78'

    def test_information(self):
        beacon = Beacon(
            name=self.beacon_name,
            uuid=self.beacon_uuid,
            long_position=51.2,
            lat_position=19.2
        )
        file = create_test_image().read()
        photo = Photo(
            filename="sukiennice",
            img=file
        )
        info = Information(
            title=self.info_title,
            content='info info test',
            beacon=beacon,
            photo=photo

        )

        self.app.db.session.add_all([beacon, info])
        self.app.db.session.commit()

        beacon_b1 = Beacon.get_by_uuid(self.beacon_uuid)
        infor_i1 = Information.get_by_name(self.info_title)
        self.assertEqual(infor_i1.title, self.info_title)
        self.assertEqual(infor_i1.content, 'info info test')
        self.assertEqual(infor_i1.beacon.name, self.beacon_name)
        self.assertEqual(infor_i1.beacon.uuid, self.beacon_uuid)

        url = ('/api/beacon/{}/information').format(beacon_b1.uuid)
        rv = self.app.get(url)
        self.assertEqual(infor_i1.title, rv.json['title'])
        self.assertEqual(infor_i1.content, rv.json['content'])
        self.assertEqual(
            infor_i1.beacon.name, rv.json['beacon']['name'])
        self.assertEqual(
            infor_i1.beacon.uuid, rv.json['beacon']['uuid'])

        url = '/api/beacon/100/information'
        self.app.get(url, status=400)
