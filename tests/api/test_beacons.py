from bguide.models.beacon import Beacon
from tests import BaseTestCase


class BeaconsTestCase(BaseTestCase):

    uuid = 'f7826da6-4fa2-4e98-8024-bc5b71e0893d'

    def test_beacons(self):
        beacon = Beacon(
            name='beacon1',
            uuid=self.uuid,
            long_position=51.2,
            lat_position=19.2
        )
        self.app.db.session.add(beacon)
        self.app.db.session.commit()
        beacon = Beacon.get_by_uuid(self.uuid)

        rv = self.app.get('/api/beacons')

        b1 = rv.json['items'][0]
        self.assertEqual('beacon1', b1['name'])
        self.assertEqual(self.uuid, b1['uuid'])
        self.assertEqual(51.2, b1['long_position'])
        self.assertEqual(19.2, b1['lat_position'])

        rv = self.app.get(('/api/beacon/{}').format(beacon.uuid))
        self.assertEqual(beacon.name, rv.json['name'])
        self.assertEqual(beacon.uuid, rv.json['uuid'])
        self.assertEqual(
            beacon.long_position, rv.json['long_position'])
        self.assertEqual(beacon.lat_position, rv.json['lat_position'])
