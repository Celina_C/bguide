from bguide.models.beacon import Beacon
from bguide.models.information import Information
from bguide.models.photo import Photo
from tests import BaseTestCase, create_test_image


class InofrmationTestCase(BaseTestCase):

    def test_info_get(self):
        beacon = Beacon(
            name='beacon1',
            uuid='number',
            long_position=51.2,
            lat_position=19.2
        )
        file = create_test_image().read()
        photo = Photo(
            filename="sukiennice",
            img=file
        )
        info = Information(
            title='Krakow',
            content='cosik cosik',
            beacon=beacon,
            photo=photo
        )
        self.app.db.session.add(info)
        self.app.db.session.commit()

        rv = self.app.get('/web/information')
        self.assertTrue('Krakow' in str(rv.body))
        self.assertTrue('cosik cosik' in str(rv.body))
        self.assertTrue('beacon1' in str(rv.body))
