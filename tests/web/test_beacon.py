from bguide import db
from bguide.models.beacon import Beacon
from tests import BaseTestCase


class BeaconWebTestCase(BaseTestCase):
    uuid = 'f7826da6-4fa2-4e98-8024-bc5b71e0893d'

    @classmethod
    def load_data(cls):
        beacon = Beacon(
            name='beacon1',
            uuid=cls.uuid,
            long_position=51.2,
            lat_position=19.2
        )
        db.session.add(beacon)
        db.session.commit()

    def test_beacon_get(self):
        rv = self.app.get('/web/beacon')
        self.assertTrue('beacon1' in str(rv.body))
        self.assertTrue(self.uuid in str(rv.body))
        self.assertTrue(str(51.2) in str(rv.body))
        self.assertTrue(str(19.2) in str(rv.body))

    def test_beacon_post(self):
        next_uuid = 'f7826da6-4fa2-4e98-8024-bc5b71e0803d'
        form = {
            'name': 'beacon2',
            'uuid': next_uuid,
            'long_position': 51.333,
            'lat_position': 19.1
        }
        data = dict(
            name='beacon2',
            uuid=next_uuid,
            long_position=51.222,
            lat_position=19.222
        )
        rv = self.app.post('/web/beacon/add', form)
        new_beacon = Beacon.get_by_uuid(next_uuid)
        # TODO no create
        # self.assertTrue(new_beacon)
        # self.assertEqual(new_beacon.name, 'beacon2')
        # self.assertEqual(new_beacon.uuid, next_uuid)
        # self.assertEqual(new_beacon.long_position, 51.333)
        # self.assertEqual(new_beacon.lat_position, 19.222)
