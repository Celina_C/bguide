import bcrypt

from bguide.models.user import User
from tests import BaseTestCase

class ModelUserTestCase(BaseTestCase):
    email = 'user1@bguide.com'
    def test_01_user_create(self):
        data = {
            'first_name': 'Ala',
            'last_name': 'Nowak',
            'email': self.email,
            'password': 'cosik',

        }
        user = User.create(**data)
        self.assertEqual(user.first_name, 'Ala')
        self.assertEqual(user.last_name, 'Nowak')
        self.assertEqual(user.email, self.email)
        self.assertTrue(user.password)

    def test_02_user_is_admin(self):
        user = User.get_by_email(self.email)
        self.assertFalse(user.admin)

        user.is_admin()

        user = User.get_by_email(self.email)
        self.assertTrue(user.admin)
