from bguide.models.beacon import Beacon
from tests import BaseTestCase


class ModelBeaconTestCase(BaseTestCase):
    uuid = 'f7826da6-4fa2-4e98-8024-bc5b71e0893d'
    next_uuid = 'e7826da6-4fa2-4e98-8024-bc5b71e0893a'

    def test_01_beacon_create(self):
        data = {
            'name': 'beacon1',
            'uuid': self.uuid,
            'long_position': 51.333,
            'lat_position': 19.111
        }

        beacon = Beacon.create(**data)

        self.assertEqual(beacon.name, 'beacon1')
        self.assertEqual(beacon.uuid, self.uuid)
        self.assertEqual(beacon.long_position, 51.333)
        self.assertEqual(beacon.lat_position, 19.111)

    def test_02_beacon_get_by_id(self):
        next_beacon = Beacon(
            name='beacon2',
            uuid=self.next_uuid,
            long_position=51.444,
            lat_position=19.333

        )
        self.app.db.session.add(next_beacon)
        self.app.db.session.commit()

        beacon1 = Beacon.get_by_id(1)
        self.assertEqual(beacon1.name, 'beacon1')
        self.assertEqual(beacon1.uuid, self.uuid)
        self.assertEqual(beacon1.long_position, 51.333)
        self.assertEqual(beacon1.lat_position, 19.111)