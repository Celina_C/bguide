from bguide.models.photo import Photo
from tests import BaseTestCase, create_test_image


class ModelPhotoTestCase(BaseTestCase):
    def test_photo_create(self):
        file = create_test_image()
        data = {
            'filename': 'photo1',
            'img': file
        }

        photo = Photo.create(**data)
        self.assertEqual(photo.filename, 'photo1')
        self.assertTrue(photo.img)

    def test_photo_get_by_id(self):
        photo= Photo.get_by_id(1)
        self.assertEqual(photo.filename, 'photo1')
        self.assertTrue(photo.img)
