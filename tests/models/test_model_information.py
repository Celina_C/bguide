from bguide import db
from bguide.models.beacon import Beacon
from bguide.models.information import Information
from bguide.models.photo import Photo
from tests import BaseTestCase, create_test_image


class ModelInformationTestCase(BaseTestCase):
    uuid = 'f7826da6-4fa2-4e98-8024-bc5b71e0893d'

    @classmethod
    def load_data(cls):
        beacon = Beacon(
            name='beacon1',
            uuid=cls.uuid,
            long_position=51.2,
            lat_position=19.2
        )
        file = create_test_image().read()
        photo = Photo(
            filename="photo1",
            img=file
        )
        db.session.add_all([beacon, photo])
        db.session.commit()

    def test_info_create(self):
        data = {
            'title': 'sukiennice',
            'content': 'text text',
        }

        info = Information.create(beacon_id=1, photo_id=1, **data)
        self.assertEqual(info.title, 'sukiennice')
        self.assertEqual(info.content, 'text text')
        self.assertEqual(info.beacon.name, 'beacon1')
        self.assertEqual(info.photo.filename, 'photo1')
